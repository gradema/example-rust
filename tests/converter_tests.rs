// https://doc.rust-lang.org/rust-by-example/testing/unit_testing.html
#[cfg(test)]
mod tests {
    use converter::binary_converter::convert_to_binary;
    #[test]
    fn test_convert_5() {
        assert_eq!(convert_to_binary(5), "101");
    }
    #[test]
    fn test_convert_10() {
        assert_eq!(convert_to_binary(10), "1010");
    }
}
