echo off
setlocal

:: Change directory to the location of the batch script
cd /d "%~dp0"

:: Create virtual environment
python -m venv .venv
if errorlevel 1 (
    echo Failed to create virtual environment
    exit /b 1
)

:: Activate virtual environment
call .venv\Scripts\activate.bat
if errorlevel 1 (
    echo Failed to activate virtual environment
    exit /b 1
)

:: Install requirements
pip install -r requirements.txt
if errorlevel 1 (
    echo Failed to install requirements
    exit /b 1
)
pip install python-magic-bin
if errorlevel 1 (
    echo Failed to install python-magic-bin
    exit /b 1
)

exit /b 0
