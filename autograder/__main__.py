import sys
from pathlib import Path

from gradema.grader.console import run_grader
from gradema.section import Section
from gradema.test import create_file_exists_test, RustProgram


def main(args: list[str]) -> int:
    print("Welcome to the autograder that uses gradema")
    test_directory = Path("autograder/test/")
    program = RustProgram("src/main.rs", "target/debug/program")
    section = Section.pointed(
        100,
        "Binary Convert Programming Assignment in Rust",
        [
            Section.pointed(5, "Compile", program.create_compile_step()),
            Section.evenly_weighted(
                "Unit Tests",
                [
                    Section.evenly_weighted(
                        "Convert 5 Unit",
                        program.create_cargo_command(["test", "--test", "converter_tests", "tests::test_convert_5"]),
                    ),
                    Section.evenly_weighted(
                        "Convert 10 Unit",
                        program.create_cargo_command(["test", "--test", "converter_tests", "tests::test_convert_10"]),
                    ),
                ],
            ),
            Section.evenly_weighted(
                "Standard Input/Output Tests",
                [
                    Section.evenly_weighted(
                        "Convert 7 Stdio",
                        program.create_traditional_stdio_test(
                            "convert_7",
                            test_directory / "stdio/inputs/decimal_7.txt",
                            test_directory / "stdio/goals/binary_7.txt",
                        ),
                    ),
                    Section.evenly_weighted(
                        "Convert 13 Stdio",
                        program.create_traditional_stdio_test(
                            "convert_13",
                            test_directory / "stdio/inputs/decimal_13.txt",
                            test_directory / "stdio/goals/binary_13.txt",
                        ),
                    ),
                ],
            ),
            Section.pointed(
                15,
                "Other Checks",
                [
                    Section.evenly_weighted(
                        "Files exist",
                        create_file_exists_test(
                            [
                                ("README.md", "ASCII text"),
                                ("gradema/test/__init__.py", "Python script, ASCII text executable"),
                                ("does not exist.txt", "ASCII text"),
                            ]
                        ),
                    ),
                    Section.evenly_weighted("Formatting", program.create_format_check()),
                ],
            ),
        ],
    )
    run_grader(args, section)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
